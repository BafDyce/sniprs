//! impl fmt::Display boilerplate

use std::fmt;

impl fmt::Display for MyStruct {
    fn fmt(&self, ff: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(ff, "{:?}", self)
    }
}
